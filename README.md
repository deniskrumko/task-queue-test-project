# Tasks queue web app

## Requirements

* Python 3.6.2
* Postgresql 9.5.8
* Django 1.11.4

## Install

1. Install and configure Postgresql database ([see manual](https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-16-04))

  > Database name: `taskqueuedatabase`

  > User: `myprojectuser`

  > Password: `password`

2. Install requirements

  ```
  pip install -r requirements.txt
  ```

3. Run migrations

  ```
  python3 manage.py migrate
  ```

## Run

```
python3 manage.py runserver
```

## Settings

* CONSTANT_INTERVAL (`int` or `False`) - specifies task duration. When it's
`false` - duration is random (from 1 to 10 seconds).

* MAX_WORKERS (`int`) - specifies amount of parallel workers. By default, it's 2.

* REMOVE_TASKS_ON_START (`bool`) - remove all `Task` from database on start.

## Known issues

* When task interval is random (`CONSTANT_INTERVAL = False`) - sometimes where
more that specified workers amount (more than 2, for example).
