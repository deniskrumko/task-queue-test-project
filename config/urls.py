from django.conf.urls import url, include
from django.contrib import admin

from apps.task_queue import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # Frontend endpoints
    url(r'^$', views.IndexView.as_view()),
    url(r'^tasks/', views.TaskQueueView.as_view()),

    # API endpoints
    url(r'^api/', include('apps.task_queue.api.urls')),
]
