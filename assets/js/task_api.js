// GET requests interval
var request_interval = 1000;

$(document).ready(function() {
  var url = '/api/tasks/';
  if (!url) {
    return;
  }
  UpdateTasks(url);
});

function UpdateTasks(url) {
  // Update tasks on page
  var text = '';

  $.get(url, function(data) {
    $("#tasks").empty();

    $.each(data, function(index, task) {
      var klass = '';
      // Add green color for running tasks
      if (task.status == 'running') {
        klass = 'green';
      }

      // Represent task data
      text = '<p class="' + klass + '"><b>ID:</b> ' +
        task.id + '&emsp;<b>Status:</b> ' +
        task.status + '&emsp;<b>Created:</b> ' +
        task.created + '&emsp;<b>Finished:</b> ' +
        task.finished + '</p>';

      $("#tasks").append(text);
    });

    if (text == '') {
      $("#tasks").append('No tasks');
    }

    // Set interval for page updating
    setTimeout(UpdateTasks, request_interval, url);
  })
}
