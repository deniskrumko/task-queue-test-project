from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class Task(models.Model):
    """Model for storing task data.

    Attributes:
        status (str): status of task.
        create_time (datetime): date of task creation.
        finish_time (datetime): date of task completion.

    """
    TASK_AWAITING = 'awaiting'
    TASK_RUNNING = 'running'
    TASK_COMPLETED = 'completed'

    TASK_STATUSES = (
        (TASK_AWAITING, _('Awaiting')),
        (TASK_RUNNING, _('Running')),
        (TASK_COMPLETED, _('Completed')),
    )

    status = models.CharField(
        choices=TASK_STATUSES,
        default=TASK_AWAITING,
        max_length=16,
        blank=False,
        verbose_name=_('Status')
    )
    create_time = models.DateTimeField(
        blank=True,
        null=True,
        auto_now_add=True,
        verbose_name=_('Create time')
    )
    finish_time = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name=_('Finish time')
    )

    def format_datetime(self, attr):
        """Method to represent datetime fields of model.

        Args:
            attr (str): name of ``Task`` attribute ('create_time' or
                'finish_time').

        Returns:
            str: date in following format - "01.01.2017 10:15:55"

        """
        time_format = '%d.%m.%Y %H:%M:%S'
        value = getattr(self, attr, None)
        return value.strftime(time_format) if value else '-'

    def start(self):
        """Shorcut to set `status` to `running`."""
        self.status = self.TASK_RUNNING
        self.save()

    def finish(self):
        """Shorcut to set `status` to `completed` and set `finish_time`."""
        self.status = self.TASK_COMPLETED
        self.finish_time = timezone.now()
        self.save()
