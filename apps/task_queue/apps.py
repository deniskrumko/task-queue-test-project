from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TaskQueueConfig(AppConfig):
    name = 'apps.task_queue'
    verbose_name = _('Task queue')

    def ready(self):
        """Prepares application"""
        from . import runner  # noqa
