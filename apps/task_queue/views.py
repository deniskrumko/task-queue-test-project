from django.views.generic import TemplateView
from django.conf import settings


class IndexView(TemplateView):
    """View for index page."""
    template_name = 'index.html'


class TaskQueueView(TemplateView):
    """View for page with tasks."""
    template_name = 'tasks.html'

    def get_task_duration(self):
        """Method to get interval of tasks."""
        constant_time = getattr(settings, 'CONSTANT_INTERVAL', False)
        if constant_time:
            return 'Constant interval ({} seconds)'.format(constant_time)

        return 'Random value (from 1 to 10 seconds)'

    def get_context_data(self, **kwargs):
        """Method to get context data."""
        return {
            'task_duration': self.get_task_duration(),
            'max_workers': getattr(settings, 'MAX_WORKERS', 2),
            'remove_tasks': getattr(settings, 'REMOVE_TASKS_ON_START', True),
        }
