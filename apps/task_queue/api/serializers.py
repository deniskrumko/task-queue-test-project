from rest_framework import serializers

from .. import models


class TaskSerializer(serializers.ModelSerializer):
    """Serializer for ``Task`` model."""
    created = serializers.SerializerMethodField()
    finished = serializers.SerializerMethodField()

    def get_finished(self, obj, **kwargs):
        return obj.format_datetime('finish_time')

    def get_created(self, obj, **kwargs):
        return obj.format_datetime('create_time')

    class Meta:
        model = models.Task
        fields = (
            'id',
            'status',
            'created',
            'finished'
        )
