from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from .. import models
from . import serializers


class TaskViewSet(ReadOnlyModelViewSet):
    """ViewSet for ``Task`` model."""
    queryset = models.Task.objects.order_by('-create_time')
    serializer_class = serializers.TaskSerializer

    def post(self, request, *args, **kwargs):
        """Method to process POST request.

        Method creates new ``Task`` instance.

        """
        models.Task.objects.create()
        return Response({}, status=status.HTTP_201_CREATED)
