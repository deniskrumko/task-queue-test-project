from rest_framework.routers import SimpleRouter

from . import views

router = SimpleRouter()

router.register(r'tasks', views.TaskViewSet)

urlpatterns = router.urls
