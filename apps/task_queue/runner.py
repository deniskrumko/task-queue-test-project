import asyncio
import random

from django.conf import settings
from threading import Thread

from .models import Task


def get_task_period():
    """Function to get time for task running."""
    constant_time = getattr(settings, 'CONSTANT_INTERVAL', False)
    return constant_time if constant_time else random.randint(0, 10)


async def task_executor(queue, loop):
    """Method to execute tasks from queue.

    Args:
        queue (Queue): instance of ``Queue`` that stores all tasks to do.
        loop (EventLoop): instance of ``EventLoop``.

    """
    while True:
        # Retrieve task from queue
        task = await queue.get()

        # Execute task
        task.start()
        await asyncio.sleep(get_task_period())
        task.finish()


async def task_observer(queue):
    """Method to add tasks to queue.

    Args:
        queue (Queue): instance of ``Queue`` that stores all tasks to do.

    """
    already_sent_tasks = []

    while True:
        for task in Task.objects.filter(status=Task.TASK_AWAITING):
            if task.id not in already_sent_tasks:
                await queue.put(task)
                already_sent_tasks.append(task.id)

        await asyncio.sleep(.1)


def start_loop(loop):
    """Function that starts loop for processing tasks queue.

    Args:
        loop (EventLoop): instance of ``EventLoop``.

    """
    asyncio.set_event_loop(loop)

    try:
        # Start workers that performs tasks
        max_workers_amount = getattr(settings, 'MAX_WORKERS', 2)
        for index in range(max_workers_amount):
            asyncio.ensure_future(task_executor(queue, loop))

        # Start worker that checks availability of tasks
        asyncio.ensure_future(task_observer(queue))

        # Run loop forever
        loop.run_forever()
    except KeyboardInterrupt:
        # Canceling pending tasks and stopping the loop
        asyncio.gather(*asyncio.Task.all_tasks()).cancel()
        # Stopping the loop
        loop.stop()
        # Received Ctrl+C
        loop.close()


# Remove all ``Task`` objects on server start
if getattr(settings, 'REMOVE_TASKS_ON_START', True):
    Task.objects.all().delete()

# Start new thread for tasks processing
loop = asyncio.new_event_loop()
queue = asyncio.Queue(loop=loop)

thread = Thread(target=start_loop, args=(loop,))
thread.start()
